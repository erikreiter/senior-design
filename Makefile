CC = g++

GUIAPP = Y
#GUIAPP = N

PROJECT = etchpcb
BINDIR = bin
OBJDIR = obj
SRCDIR = src
OUTDIR = output

$(PROJECT): GuiWindow Interpreter Point RpiGPIO Stepper
	cd $(SRCDIR); make
ifeq ($(GUIAPP), Y)
	$(CC) $(OBJDIR)/*.o -o $(BINDIR)/$(PROJECT) `pkg-config gtkmm-3.0 --libs` -lX11
else
	$(CC) $(OBJDIR)/*.o -o $(BINDIR)/$(PROJECT)
endif

GuiWindow:
	cd $(SRCDIR)/Gui; make
Interpreter:
	cd $(SRCDIR)/Interpreter; make
Point:
	cd $(SRCDIR)/Point; make
RpiGPIO:
	cd $(SRCDIR)/RpiGPIO; make
Stepper:
	cd $(SRCDIR)/Stepper; make

all: $(PROJECT)

clean:
	find . -type f | xargs -n 5 touch
	rm $(OBJDIR)/*.o
	rm $(BINDIR)/*
	rm $(OUTDIR)/*
