#include <sstream>
#include <string>

#include "Point.h" 

using namespace std;

int Point::pcount = 0;

Point::Point()
{
    x = 0; y = 0; pcount++;
}

Point::Point(double xx, double yy)
{
    x = xx; y = yy; pcount++;
}

string Point::toString()
{
    stringstream ss;
    ss<<"("<<x<<","<<y<<")";
    return ss.str();
}

double Point::getX()
{
    return x;
}

 double Point::getY()
{
    return y;
}
void Point::setX(double xx)
{
    x = xx;
}
void Point::setY(double yy)
{
    y = yy;
}

void Point::setXY(double xx, double yy)
{
    x = xx;  y = yy;
}

int Point::getPointCount()
{
    return pcount;
}

Point::Point(Point &other)
{
    x = other.x;  y = other.y;  pcount++;
}

Point::~Point()
{
    pcount--;
}


bool Point::isEqual(Point other)
{
    if (x!=other.x)
        return false;
    return y==other.y;
}

bool Point::operator==(Point &rhs)
{
    if (x!=rhs.x)
        return false;
    return y==rhs.y;
}

void Point::operator=(Point &rhs)
{
    x = rhs.x;
    y = rhs.y;
}

