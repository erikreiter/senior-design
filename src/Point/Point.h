// Point.h -- class for euclidean points
#ifndef POINT_H
#define POINT_H
#include <string>

using namespace std;

class Point
{
    public:
        Point();
        Point(double,double);
        Point(Point &);
        ~Point();

        string toString();
        double getX();
        double getY();
        void setX(double);
        void setY(double);
        void setXY(double,double);

        bool isEqual(Point);
        bool operator==(Point &);
        void operator=(Point &);

        static int getPointCount();

    private:
        double x,y;
        static int pcount;
   };
#endif
