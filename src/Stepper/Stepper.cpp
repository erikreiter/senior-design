//Stepper.cpp -- stepper motor class implementation
#include <string>
#include <cstdlib>
#include <unistd.h>
#include <iostream>

#include "Stepper.h"
#include "../RpiGPIO/RpiGPIO.h"

using namespace std;

Stepper::Stepper(int steps, int pin1, int pin2, int pin3, int pin4)
{
    this->step_num = 0;
    this->speed = 0;
    this->direction = 0;
    this->steps = steps;
    this->half_step = false;

    //set up GPIO pins
    this->pin1 = new RpiGPIO(pin1, "out");
    this->pin2 = new RpiGPIO(pin2, "out");
    this->pin3 = new RpiGPIO(pin3, "out");
    this->pin4 = new RpiGPIO(pin4, "out");

    this->pin1->setDir("out");
    this->pin2->setDir("out");
    this->pin3->setDir("out");
    this->pin4->setDir("out");

    this->pin1->setVal(0);
    this->pin2->setVal(0);
    this->pin3->setVal(0);
    this->pin4->setVal(0);
}

Stepper::Stepper(int steps, bool half_step, int pin1, int pin2, int pin3, int pin4)
{
    this->half_step = half_step;
    this->step_num = 0;
    this->speed = 0;
    this->direction = 0;
    this->steps = steps;

    //set up GPIO pins
    this->pin1 = new RpiGPIO(pin1, "out");
    this->pin2 = new RpiGPIO(pin2, "out");
    this->pin3 = new RpiGPIO(pin3, "out");
    this->pin4 = new RpiGPIO(pin4, "out");

    this->pin1->setVal(0);
    this->pin2->setVal(0);
    this->pin3->setVal(0);
    this->pin4->setVal(0);
}

Stepper::~Stepper()
{
    delete this->pin1;
    delete this->pin2;
    delete this->pin3;
    delete this->pin4;
}

void Stepper::setSpeed(int rpm)
{
    this->speed = rpm;
    //calculate delay (in us) per step
    this->delay = (60L * 1000000L) / (this->speed * this->steps);
    cout<<"speed = "<<this->speed<<" rpm... delay = "<<this->delay<<" us"<<endl;
}

void Stepper::step(int steps)
{
    int steps_remaining;

    if (steps < 0)
        this->direction = -1;
    else
        this->direction = 1;

    steps_remaining = abs(steps);
    if (this->half_step) {
        while (steps_remaining > 0) {
            if (this->direction == 1) { //step in (+) direction
                this->step_num++;
                if (this->step_num > 7)
                    this->step_num = 0;
                } else if (this->direction == -1) { //step in (-) direction
                    this->step_num--;
                if (this->step_num < 0)
                    this->step_num = 7;
            }

            switch (this->step_num) {
            case 0:
                this->pin1->setVal(1);
                this->pin2->setVal(0);
                this->pin3->setVal(1);
                this->pin4->setVal(0);
                break;
            case 1:
                this->pin1->setVal(0);
                this->pin2->setVal(0);
                this->pin3->setVal(1);
                this->pin4->setVal(0);
                break;
            case 2:
                this->pin1->setVal(0);
                this->pin2->setVal(1);
                this->pin3->setVal(1);
                this->pin4->setVal(0);
                break;
            case 3:
                this->pin1->setVal(0);
                this->pin2->setVal(1);
                this->pin3->setVal(0);
                this->pin4->setVal(0);
                break;
            case 4:
                this->pin1->setVal(0);
                this->pin2->setVal(1);
                this->pin3->setVal(0);
                this->pin4->setVal(1);
                break;
            case 5:
                this->pin1->setVal(0);
                this->pin2->setVal(0);
                this->pin3->setVal(0);
                this->pin4->setVal(1);
                break;
            case 6:
                this->pin1->setVal(1);
                this->pin2->setVal(0);
                this->pin3->setVal(0);
                this->pin4->setVal(1);
                break;
            case 7:
                this->pin1->setVal(1);
                this->pin2->setVal(0);
                this->pin3->setVal(0);
                this->pin4->setVal(0);
                break;
            }
            steps_remaining--;
            //shouldn't need this delay, controlling stepper speed in iterpreter
            //usleep(this->delay);
        }
    } else {
        while (steps_remaining > 0) {
            if (this->direction == 1) { //step in (+) direction
                this->step_num++;
                if (this->step_num > 3)
                    this->step_num = 0;
                } else if (this->direction == -1) { //step in (-) direction
                    this->step_num--;
                if (this->step_num < 0)
                    this->step_num = 3;
            }

            switch (this->step_num) {
            case 0:
                this->pin1->setVal(1);
                this->pin2->setVal(0);
                this->pin3->setVal(1);
                this->pin4->setVal(0);
                break;
            case 1:
                this->pin1->setVal(0);
                this->pin2->setVal(1);
                this->pin3->setVal(1);
                this->pin4->setVal(0);
                break;
            case 2:
                this->pin1->setVal(0);
                this->pin2->setVal(1);
                this->pin3->setVal(0);
                this->pin4->setVal(1);
                break;
            case 3:
                this->pin1->setVal(1);
                this->pin2->setVal(0);
                this->pin3->setVal(0);
                this->pin4->setVal(1);
                break;
            }
            steps_remaining--;
            //shouldn't need this delay, controlling stepper speed in iterpreter
            //usleep(this->delay);
        }
    }
}

void Stepper::killPower(void)
{
    this->pin1->setVal(0);
    this->pin2->setVal(0);
    this->pin3->setVal(0);
    this->pin4->setVal(0);
}

