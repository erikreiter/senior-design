//Stepper.h -- stepper motor class definition
#ifndef STEPPER_H
#define STEPPER_H
#include <string>

#include "../RpiGPIO/RpiGPIO.h"

class Stepper
{
    public:
        Stepper(int steps, int pin1, int pin2, int pin3, int pin4);
        Stepper(int steps, bool half_step, int pin1, int pin2, int pin3, int pin4);
        ~Stepper();
        void setSpeed(int rpm);
        void step(int steps);
        void killPower(void);

    private:
        int direction; //direction of rotation
        int speed; //RPMs
        bool half_step;
        int step_num; //which step we're on
        int steps;
        unsigned int delay; //delay between steps

        //motor pins
        RpiGPIO *pin1;
        RpiGPIO *pin2;
        RpiGPIO *pin3;
        RpiGPIO *pin4;
};
#endif
