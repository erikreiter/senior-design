#include "Gui/GuiWindow.h"

#include <X11/Xlib.h>

int main(int argc, char *argv[])
{
    XInitThreads();

    Glib::thread_init();

    Glib::RefPtr<Gtk::Application> app = Gtk::Application::create(argc, argv, "com.gtkmm.etchpcb.base");

    GuiWindow window;

    return app->run(window);
}
