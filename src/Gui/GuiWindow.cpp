#include <cstdlib>
#include <fstream>
#include <iostream>
#include <string>
#include <sstream>

#include "GuiWindow.h"
#include "../Interpreter/Interpreter.h"
#include "../RpiGPIO/RpiGPIO.h"
#include "../Stepper/Stepper.h"

#define GERBER2GCODE_PROG_NAME "pcb2gcode"
#define OUTPUT_PATH "./output"
#define OUTPUT_IMAGE0_PREFIX "outp0_original_"
#define OUTPUT_IMAGE1_PREFIX "outp1_original_"

using namespace std;

GuiWindow::GuiWindow()
{
    this->workerThread_etchTop_active = false;
    this->workerThread_etchBottom_active = false;
    this->workerThread_displayPcb2gcode_active = false;
    this->stop_etching = false;

    //window properties
    set_title("Senior Design 2016 -- PCB Milling Machine");
    set_default_size(600, 360);

    //vbox
    Gtk::Box *vbox = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_VERTICAL, 0));
    add(*vbox);

    //grid
    Gtk::Grid *grid = Gtk::manage(new Gtk::Grid);
    grid->set_border_width(5);
    vbox->add(*grid);

    //millproject file chooser button
    Gtk::Label *label_millproject = Gtk::manage(new Gtk::Label("millproject File:"));
    label_millproject->set_hexpand(true);
    label_millproject->set_vexpand(false);
    grid->attach(*label_millproject, 0 ,0, 1, 1);
    fileButton_millproject = Gtk::manage(new Gtk::FileChooserButton());
    fileButton_millproject->set_hexpand(true);
    fileButton_millproject->set_vexpand(false);
    fileButton_millproject->signal_file_set().connect(sigc::mem_fun(*this, &GuiWindow::on_fileButton_millproject_set));
    grid->attach(*fileButton_millproject, 0 ,1, 1, 1);

    //top gerber file chooser button
    Gtk::Label *label_topGerber = Gtk::manage(new Gtk::Label("Top Layer Gerber File:"));
    label_topGerber->set_hexpand(true);
    label_topGerber->set_vexpand(false);
    grid->attach(*label_topGerber, 0 ,2, 1, 1);
    fileButton_topGerber = Gtk::manage(new Gtk::FileChooserButton());
    fileButton_topGerber->set_hexpand(true);
    fileButton_topGerber->set_vexpand(false);
    fileButton_topGerber->signal_file_set().connect(sigc::mem_fun(*this, &GuiWindow::on_fileButton_topGerber_set));
    grid->attach(*fileButton_topGerber, 0 ,3, 1, 1);

    //bottom gerber file chooser button
    Gtk::Label *label_bottomGerber = Gtk::manage(new Gtk::Label("Bottom Layer Gerber File:"));
    label_bottomGerber->set_hexpand(true);
    label_bottomGerber->set_vexpand(false);
    grid->attach(*label_bottomGerber, 0 ,4, 1, 1);
    fileButton_bottomGerber = Gtk::manage(new Gtk::FileChooserButton());
    fileButton_bottomGerber->set_hexpand(true);
    fileButton_bottomGerber->set_vexpand(false);
    fileButton_bottomGerber->signal_file_set().connect(sigc::mem_fun(*this, &GuiWindow::on_fileButton_bottomGerber_set));
    grid->attach(*fileButton_bottomGerber, 0 ,5, 1, 1);

    //generate gcode button
    Gtk::Button *button_generateGCode = Gtk::manage(new Gtk::Button("Generate GCode"));
    button_generateGCode->set_hexpand(true);
    button_generateGCode->set_vexpand(false);
    button_generateGCode->signal_clicked().connect(sigc::mem_fun(*this, &GuiWindow::on_button_generateGCode_click));
    grid->attach(*button_generateGCode, 1, 0, 1, 2);

    //etch top layer button
    Gtk::Button *button_etchTopLayer = Gtk::manage(new Gtk::Button("Etch Top"));
    button_etchTopLayer->set_hexpand(true);
    button_etchTopLayer->set_vexpand(false);
    button_etchTopLayer->signal_clicked().connect(sigc::mem_fun(*this, &GuiWindow::on_button_etchTopLayer_click));
    grid->attach(*button_etchTopLayer, 1, 2, 1, 2);

    //etch bottom layer button
    Gtk::Button *button_etchBottomLayer = Gtk::manage(new Gtk::Button("Etch Bottom"));
    button_etchBottomLayer->set_hexpand(true);
    button_etchBottomLayer->set_vexpand(false);
    button_etchBottomLayer->signal_clicked().connect(sigc::mem_fun(*this, &GuiWindow::on_button_etchBottomLayer_click));
    grid->attach(*button_etchBottomLayer, 1, 4, 1, 2);

    //etch layer progress bar
    progressBar_etchLayer.set_hexpand(true);
    progressBar_etchLayer.set_vexpand(false);
    progressBar_etchLayer.set_fraction(0.0);
    grid->attach(progressBar_etchLayer, 0, 6, 2, 2);

    //Stop etch button
    Gtk::Button *button_stopEtching = Gtk::manage(new Gtk::Button("STOP"));
    button_stopEtching->set_hexpand(true);
    button_stopEtching->set_vexpand(false);
    button_stopEtching->signal_clicked().connect(sigc::mem_fun(*this, &GuiWindow::on_button_stop_etch_click));
    grid->attach(*button_stopEtching, 0, 8, 2, 1);

    //console output window
    Gtk::Frame *frame_consoleOutput = Gtk::manage(new Gtk::Frame("Console Output:"));
    frame_consoleOutput->set_hexpand(true);
    frame_consoleOutput->set_vexpand(true);
    grid->attach(*frame_consoleOutput, 0, 9, 2, 1);
    Gtk::ScrolledWindow *scrolledWindow_consoleOutPut = Gtk::manage(new Gtk::ScrolledWindow());
    scrolledWindow_consoleOutPut->set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC);
    textView_consoleOutput = Gtk::manage(new Gtk::TextView());
    scrolledWindow_consoleOutPut->add(*textView_consoleOutput);
    frame_consoleOutput->add(*scrolledWindow_consoleOutPut);

    //setup threads
    workerThread_etchLayer_disp.connect(sigc::mem_fun(*this, &GuiWindow::on_workerThread_etchLayer_event));
    workerThread_displayPcb2gcode_disp.connect(sigc::mem_fun(*this, &GuiWindow::on_workerThread_updateConsoleOut_event));

    vbox->show_all();
}

GuiWindow::~GuiWindow()
{
    this->workerThread_etchTop_active = false;
    this->workerThread_etchBottom_active = false;
    this->workerThread_displayPcb2gcode_active = false;
}

//button click functions
void GuiWindow::on_button_generateGCode_click(void)
{
    this->topSuccessfullyConverted = false;
    this->bottomSuccessfullyConverted = false;

    if (this->filePath_millproject == "") {
        dialog_error("millproject file not imported");
        return;
    }
    if (this->filePath_topGerber == "" &&
        this->filePath_bottomGerber == "") {
        dialog_error("No files imported");
        return;
    }

    if (this->workerThread_displayPcb2gcode_active == true) {
        dialog_error("Gerber to GCode convert already in progress");
        return;
    } else if (this->workerThread_etchTop_active || this->workerThread_etchBottom_active) {
        dialog_error("Cannot convert to Gcode...Etch in progress");
    } else {
        this->workerThread_displayPcb2gcode_active = true;
        workerThread_displayPcb2gcode = Glib::Thread::create(sigc::mem_fun(*this, &GuiWindow::display_pcb2gcode_out));
    }

    //clear output directory
    string cmdStr = "rm " + (string) OUTPUT_PATH + "/*.ngc";
    system(cmdStr.c_str());
    cmdStr = "rm " + (string) OUTPUT_PATH + "/*.png";
    system(cmdStr.c_str());

    //build pcb2gcode cmd
    string pcb2gcodeFilePath = (string) OUTPUT_PATH + "/pcb2gcode.out";
    cmdStr = (string) GERBER2GCODE_PROG_NAME + " --output-dir " + (string) OUTPUT_PATH + " ";
    if (this->filePath_topGerber != "") {
        cmdStr += "--front " + this->filePath_topGerber;
        this->topSuccessfullyConverted = true;
    } if (this->filePath_bottomGerber != "") {
        cmdStr += " --back " + this->filePath_bottomGerber;
        this->bottomSuccessfullyConverted = true;
    }

    cmdStr += "> " + pcb2gcodeFilePath + " 2>&1";

    system(cmdStr.c_str());
}

void GuiWindow::on_button_etchTopLayer_click(void)
{
    if (this->filePath_topGerber == "") {
        dialog_error("Top layer gerber not imported");
        return;
    }

    if (this->stop_etching) {
        this->stop_etching = false;
    } else if (this->workerThread_etchBottom_active) {
        dialog_error("Already etching bottom layer");
    } else if (this->workerThread_etchTop_active) {
        dialog_error("Already etching top layer");
    } else if (!this->topSuccessfullyConverted) {
        dialog_error("Top layer was not converted to G-Code");
    } else {
        this->workerThread_etchTop_active = true;
        workerThread_etchTop = Glib::Thread::create(sigc::mem_fun(*this, &GuiWindow::etch_top_layer));
    }
}

void GuiWindow::on_button_etchBottomLayer_click(void)
{
    if (this->filePath_bottomGerber == "") {
        dialog_error("Bottom layer gerber not imported");
        return;
    }

    if (this->stop_etching) {
        this->stop_etching = false;
    } else if (this->workerThread_etchBottom_active) {
        dialog_error("Already etching bottom layer");
    } else if (this->workerThread_etchTop_active) {
        dialog_error("Already etching top layer");
    } else if (!this->bottomSuccessfullyConverted) {
        dialog_error("Bottom layer was not converted to G-Code");
    } else {
        this->workerThread_etchBottom_active = true;
        workerThread_etchBottom = Glib::Thread::create(sigc::mem_fun(*this, &GuiWindow::etch_bottom_layer));
    }
}

void GuiWindow::on_button_stop_etch_click(void)
{
    this->stop_etching = true;
}

//file button functions
void GuiWindow::on_fileButton_millproject_set(void)
{
    //get file path
    this->filePath_millproject = fileButton_millproject->get_filename();

    //check that correct file chosen
    size_t pos = this->filePath_millproject.rfind("millproject");

    //set string to empty if not correct file
    if (pos == string::npos) {
        fileButton_millproject->unselect_filename(this->filePath_millproject);
        this->filePath_millproject = "";
        dialog_error("invalid millproject file chosen");
    }
}

void GuiWindow::on_fileButton_topGerber_set(void)
{
    this->filePath_topGerber = fileButton_topGerber->get_filename();

    if (this->filePath_bottomGerber != "")
        this->topAndBottomSet = true;
    else
        this->topAndBottomSet = false;
}

void GuiWindow::on_fileButton_bottomGerber_set(void)
{
    this->filePath_bottomGerber = fileButton_bottomGerber->get_filename();

    if (this->filePath_bottomGerber != "")
        this->topAndBottomSet = true;
    else
        this->topAndBottomSet = false;
}

void GuiWindow::on_workerThread_etchLayer_event(void)
{
    this->workerThread_etchLayer_event_done = false;

    //update console output buffer
    stringstream ss;
    ss<<"Completed Gcode Command "<<this->current_gcode_cmd<<"/"<<this->total_gcode_cmd;
    console_write_over(ss.str());
    ss.clear();
    ss.str("");

    //update progress bar
    progressBar_etchLayer.set_fraction(this->current_gcode_cmd / this->total_gcode_cmd);

    this->workerThread_etchLayer_event_done = true;
}

void GuiWindow::on_workerThread_updateConsoleOut_event(void)
{
    if (this->console_writeBuffer != "")
        console_write(this->console_writeBuffer);

    this->console_writeBuffer = "";
}

//misc functions
void GuiWindow::dialog_error(Glib::ustring message)
{
    Gtk::MessageDialog error(message, false, Gtk::MESSAGE_ERROR, Gtk::BUTTONS_OK, true);
    error.set_title("Error");
    error.run();
}

void GuiWindow::dialog_image(Gtk::Image *image, string message)
{
    Gtk::Dialog dialog(message, false);
    Gtk::Box *vbox = dialog.get_vbox();
    vbox->add(*image);
    vbox->show_all();
    dialog.run();
}

void GuiWindow::console_write(Glib::ustring message)
{
    this->textView_consoleOutput->get_buffer()->insert_at_cursor(message + "\n");
}

void GuiWindow::console_write_over(Glib::ustring message)
{
    Glib::RefPtr<Gtk::TextBuffer> buffer =  this->textView_consoleOutput->get_buffer();

    buffer->erase(buffer->get_iter_at_line(buffer->get_line_count()), buffer->end());
    this->textView_consoleOutput->get_buffer()->insert_at_cursor(message);
}

void GuiWindow::display_pcb2gcode_out(void)
{
    string pcb2gcodeFilePath = (string) OUTPUT_PATH + "/pcb2gcode.out";

    //clear contents of file (if any)
    fstream output_file(pcb2gcodeFilePath.c_str(), ios::out | ios::trunc);
    if (output_file) {
        output_file.close();
    } else {
        cout<<"Error opening "<<pcb2gcodeFilePath<<endl;
        this->workerThread_displayPcb2gcode_active = false;
        return;
    }

    this->console_writeBuffer = "";
    string lastline = "";
    while (this->workerThread_displayPcb2gcode_active) {
        output_file.open(pcb2gcodeFilePath.c_str(), ios::in);
        if (output_file.is_open()) {
            string line = "";
            while (getline(output_file, line)) {
                if (line != lastline) {
                    this->console_writeBuffer = line;
                    workerThread_displayPcb2gcode_disp();
                    while (this->console_writeBuffer != ""); //wait for buffer to be cleared (indicating write complete)
                }
                if (line.find("END.") != string::npos) {
                    this->workerThread_displayPcb2gcode_active = false;
                    break;
                }
            }
            output_file.close();
        }
    }

    //display output images
    try {
        if (this->topAndBottomSet) {
            //display top image
            string imagePath = (string) OUTPUT_PATH + "/" + (string) OUTPUT_IMAGE1_PREFIX + "front.png";
            image_topLayer = Gtk::manage(new Gtk::Image(Gdk::Pixbuf::create_from_file(imagePath, 600, 600, true)));
            dialog_image(this->image_topLayer, "Top Layer Output");

            //display bottom image
            imagePath = (string) OUTPUT_PATH + "/" + (string) OUTPUT_IMAGE0_PREFIX + "back.png";
            image_bottomLayer = Gtk::manage(new Gtk::Image(Gdk::Pixbuf::create_from_file(imagePath, 600, 600, true)));
            dialog_image(this->image_bottomLayer, "Bottom Layer Output");
        } else if (this->filePath_topGerber != "") {
            string imagePath = (string) OUTPUT_PATH + "/" + (string) OUTPUT_IMAGE0_PREFIX + "front.png";
            image_topLayer = Gtk::manage(new Gtk::Image(Gdk::Pixbuf::create_from_file(imagePath, 600, 600, true)));
            dialog_image(this->image_topLayer, "Top Layer Output");
        } else if (this->filePath_bottomGerber != "") {
            string imagePath = (string) OUTPUT_PATH + "/" + (string) OUTPUT_IMAGE0_PREFIX + "back.png";
            image_bottomLayer = Gtk::manage(new Gtk::Image(Gdk::Pixbuf::create_from_file(imagePath, 600, 600, true)));
            dialog_image(this->image_bottomLayer, "Bottom Layer Output");
        }
    } catch (Glib::Error e) {
        console_write("Failed to open output images\n");
    }
}

void GuiWindow::etch_top_layer(void)
{
    etch_layer((string) OUTPUT_PATH + "/front.ngc", &this->workerThread_etchTop_active);
    this->workerThread_etchTop_active = false;
}

void GuiWindow::etch_bottom_layer(void)
{
    etch_layer((string) OUTPUT_PATH + "/back.ngc", &this->workerThread_etchBottom_active);
    this->workerThread_etchTop_active = false;
}

void GuiWindow::etch_layer(string gcode_file, bool thread_active)
{
    string currentline;
    ifstream f;

    this->current_gcode_cmd = 0;
    this->total_gcode_cmd = 0;

    f.open(gcode_file.c_str());
    if (!f.is_open()) {
        dialog_error("Could not open file: " + gcode_file);
        thread_active = false;
        return;
    } else {
        //get total number of commands
        while (getline(f, currentline)) {
            if (currentline != "")
                this->total_gcode_cmd++;
        }

        //return to begining of file stream
        f.clear();
        f.seekg(0, ios::beg);
    }

    //pin initialization
    Stepper *stepx = new Stepper(200, 6, 13, 19, 26);
    Stepper *stepy = new Stepper(200, 12, 16, 20, 21);
    Stepper *stepz = new Stepper(200, 22, 10, 9, 11);
    RpiGPIO *switchx = new RpiGPIO(4, "in");
    RpiGPIO *switchy = new RpiGPIO(17, "in");
    RpiGPIO *switchz = new RpiGPIO(27, "in");
    RpiGPIO *drillToolEnb = new RpiGPIO(14, "out");

    Interpreter gcode_interpreter(stepx, stepy, stepz,
                                 switchx, switchy, switchz,
                                 drillToolEnb);

    //proccess gcode file
    while (getline(f, currentline)) {
        while (this->stop_etching); //if etch has been stopped, wait until resumed
        if (currentline != "") {
            this->current_gcode_cmd++;
            gcode_interpreter.processCmd(currentline);
            workerThread_etchLayer_disp();
            //wait for GUI thread to finish updating
            while(!this->workerThread_etchLayer_event_done);
        }
    }

    //NOTE: steppers and gpio pointers deallocated in Interpreter destructor
}
