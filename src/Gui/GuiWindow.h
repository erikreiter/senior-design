#ifndef GUIWINDOW_H
#define GUIWINDOW_H

#include <gtkmm.h>
#include <iostream>
#include <string>

using namespace std;

class GuiWindow : public Gtk::Window
{
    public:
        GuiWindow();
        virtual ~GuiWindow();

    protected:
        //file checker buttons
        Gtk::FileChooserButton *fileButton_millproject;
        Gtk::FileChooserButton *fileButton_topGerber;
        Gtk::FileChooserButton *fileButton_bottomGerber;

        //console output
        Gtk::TextView *textView_consoleOutput;

        //Dispatchers
        Glib::Dispatcher workerThread_etchLayer_disp;
        Glib::Dispatcher workerThread_displayPcb2gcode_disp;

        //images
        Gtk::Image *image_topLayer;
        Gtk::Image *image_bottomLayer;

    private:
        bool stop_etching;
        //progress bar
        Gtk::ProgressBar progressBar_etchLayer;

        string filePath_millproject;
        string filePath_topGerber;
        string filePath_bottomGerber;
        bool topAndBottomSet;
        bool topSuccessfullyConverted;
        bool bottomSuccessfullyConverted;

        //Event handlers
        void on_button_generateGCode_click();
        void on_button_etchTopLayer_click();
        void on_button_etchBottomLayer_click();
        void on_button_stop_etch_click();
        void on_fileButton_millproject_set();
        void on_fileButton_topGerber_set();
        void on_fileButton_bottomGerber_set();
        void on_workerThread_etchLayer_event();
        void on_workerThread_updateConsoleOut_event();

        //Worker Threads
        Glib::Thread *workerThread_displayPcb2gcode;
        Glib::Thread *workerThread_etchTop;
        Glib::Thread *workerThread_etchBottom;

        //Thread management
        bool workerThread_displayPcb2gcode_active;
        bool workerThread_etchTop_active;
        bool workerThread_etchBottom_active;
        bool workerThread_etchLayer_event_done;
        double current_gcode_cmd;
        double total_gcode_cmd;
        string console_writeBuffer;

        void display_pcb2gcode_out();
        void etch_top_layer();
        void etch_bottom_layer();
        void etch_layer(string, bool);

        void dialog_error(Glib::ustring); //prints message to dialog window
        void dialog_image(Gtk::Image *, string message);
        void console_write(Glib::ustring); //prints message to console window
        void console_write_over(Glib::ustring); //prints message to console window, but erases last line first
};
#endif //GUIWINDOW_H
