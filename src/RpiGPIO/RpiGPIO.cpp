#include <cstdlib>
#include <string>
#include <iostream>
#include <sys/mman.h>
#include <fcntl.h>

#include "RpiGPIO.h"

#define GPIO_PATH "/sys/class/gpio/"
#define INVERT_OUTPUT 0

#define BCM2708_PERI_BASE 0x3F000000
#define GPIO_BASE (BCM2708_PERI_BASE + 0x200000)
#define PAGE_SIZE (4*1024)
#define BLOCK_SIZE (4*1024)

//gpio macros
#define INP_GPIO(pin) *(gpio+((pin)/10)) &= ~(7<<(((pin)%10)*3))
#define OUT_GPIO(pin) *(gpio+((pin)/10)) |= (1<<(((pin)%10)*3))
#define SET_GPIO_ALT(pin,alt) *(gpio+(((pin)/10))) |= (((alt)<=3?(alt)+4:(alt)==4?3:2)<<(((pin)%10)*3))
#define GPIO_SET *(gpio+7)
#define GPIO_CLR *(gpio+10)
#define GET_GPIO(pin) (*(gpio+13)&(1<<pin))

using namespace std;

RpiGPIO::RpiGPIO(int pin)
{
    if ((this->mem_fd = open("/dev/mem", O_RDWR | O_SYNC)) < 0) {
        cout<<"ERROR: falied to open /dev/mem"<<endl;
        exit(-1);
    }

    this->gpio_map = mmap(NULL, BLOCK_SIZE, (PROT_READ | PROT_WRITE),
                          MAP_SHARED, this->mem_fd, GPIO_BASE);

    if (gpio_map == MAP_FAILED) {
        cout<<"mmap error "<<(long)gpio_map<<endl;
        exit(-1);
    }

    gpio = (volatile unsigned *) gpio_map;

    this->pin = pin;
}

RpiGPIO::RpiGPIO(int pin, string dir)
{
    if ((this->mem_fd = open("/dev/mem", O_RDWR | O_SYNC)) < 0) {
        cout<<"ERROR: falied to open /dev/mem"<<endl;
        exit(-1);
    }

    this->gpio_map = mmap(NULL, BLOCK_SIZE, (PROT_READ | PROT_WRITE),
                          MAP_SHARED, this->mem_fd, GPIO_BASE);

    if (gpio_map == MAP_FAILED) {
        cout<<"mmap error "<<(long)gpio_map<<endl;
        exit(-1);
    }

    gpio = (volatile unsigned *) gpio_map;

    this->pin = pin;
    this->dir = dir;

    if (dir == "in")
        setDir("in");
    else
        setDir("out");
}

RpiGPIO::~RpiGPIO()
{
    if (this->dir == "out")
        setVal(0);

    munmap(this->gpio_map, BLOCK_SIZE);
}

bool RpiGPIO::setDir(string dir)
{
    INP_GPIO(this->pin);
    if (dir == "out") {
        OUT_GPIO(this->pin);
    } else if (dir != "in") {
        cout<<"ERROR: Failed to set dir "<<dir<<" on pin"<<this->pin<<endl;
        return false;
    }

    return true;
}

void RpiGPIO::setVal(int val)
{
#if INVERT_OUTPUT
    if (val == 0)
        val = 1;
    else
        val = 0;
#endif
    if (val)
        GPIO_SET = 0x01 << this->pin;
    else
        GPIO_CLR = 0x01 << this->pin;
}

int RpiGPIO::getVal(void)
{
    if (GET_GPIO(this->pin))
        return 1;
    else
        return 0;
}
