#ifndef RPIGPIO_H
#define RPIGPIO_H

#include <string>

using namespace std;

class RpiGPIO
{
    public:
        RpiGPIO(int pin);
        RpiGPIO(int pin, string dir);
        ~RpiGPIO();

        bool setDir(string dir);
        void setVal(int val);
        int getVal();

    private:
        int pin;
        string dir;
        int mem_fd;
        void *gpio_map;
        volatile unsigned *gpio;
};

#endif
