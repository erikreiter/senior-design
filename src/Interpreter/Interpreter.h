// Interpreter.h -- G-Code interpreter class definition
#ifndef INTERPRETER_H
#define INTERPRETER_H
#include <string>

#include "../Stepper/Stepper.h"
#include "../Point/Point.h"
#include "../RpiGPIO/RpiGPIO.h"

//how many units per step
//full step
#define UNITS_PER_STEP_XY 0.000246
#define UNITS_PER_STEP_Z  0.000246
//half step
//#define UNITS_PER_STEP_XY 0.000123
//#define UNITS_PER_STEP_Z  0.000123

#define DIST_MD_ABS     90 //absolute coordinates/distance
#define FEEDRATE_MD_UPM 94 //feedrate in units per minute
#define UNITS_INCH      20 //units = inches
#define UNITS_MM        21 //units = mm

class Interpreter
{
    public:
        Interpreter(Stepper *x, Stepper *y, RpiGPIO *xswitch, RpiGPIO *yswitch, RpiGPIO *drillToolEnb);
        Interpreter(Stepper *x, Stepper *y, Stepper *z, RpiGPIO *xswitch, RpiGPIO *yswitch,
                    RpiGPIO *zswitch, RpiGPIO *drillToolEnd);
        ~Interpreter();
        bool processCmd(string cmd);
        void returnHome(void);

    private:
        Stepper *sX;
        Stepper *sY;
        Stepper *sZ;
        RpiGPIO *xhome;
        RpiGPIO *yhome;
        RpiGPIO *zhome;
        RpiGPIO *drillToolEnb;
        Point pos;
        Point xyError;
        double pos_z;
        double zError;
        long step_delay_xy; //delay b/w steps in us
        long step_delay_z; //delay b/w steps in us

        //parameters
        int dist_mode;
        double feedrate;
        int feedrate_mode;
        int units;

        void moveXY(Point next);
        void moveZ(double next_z);
        void setFeedrate(double fr);
};
#endif
