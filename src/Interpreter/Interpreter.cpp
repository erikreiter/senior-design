// Interpreter.cpp -- G-Code interpreter class implementation
#include <cmath>
#include <iostream>
#include <string>
#include <sstream>
#include <unistd.h>

#include "Interpreter.h"
#include "../Point/Point.h"
#include "../RpiGPIO/RpiGPIO.h"
#include "../Stepper/Stepper.h"

#define DEBUG 1
#define CHECK_LIMIT_SW 1

using namespace std;

Interpreter::Interpreter(Stepper *x, Stepper *y, RpiGPIO *xswitch, RpiGPIO *yswitch, RpiGPIO *drillToolEnb)
{
    //default to absolute mode, inches, and inches/min
    this->dist_mode = DIST_MD_ABS;
    this->units = UNITS_INCH;
    this->feedrate_mode = FEEDRATE_MD_UPM;
    this->sX = x;
    this->sY = y;
    this->sZ = NULL;
    this->drillToolEnb = drillToolEnb;
    this->xhome = xswitch;
    this->yhome = yswitch;
    this->zhome = NULL;
    returnHome();
    this->pos.setX(0.0);
    this->pos.setY(0.0);
    this->xyError.setX(0.0);
    this->xyError.setY(0.0);
}

Interpreter::Interpreter(Stepper *x, Stepper *y, Stepper *z, RpiGPIO *xswitch, RpiGPIO *yswitch,
                         RpiGPIO *zswitch, RpiGPIO *drillToolEnb)
{
    //default to absolute mode, inches, and inches/min
    this->dist_mode = DIST_MD_ABS;
    this->units = UNITS_INCH;
    this->feedrate_mode = FEEDRATE_MD_UPM;
    this->sX = x;
    this->sY = y;
    this->sZ = z;
    this->drillToolEnb = drillToolEnb;
    this->xhome = xswitch;
    this->yhome = yswitch;
    this->zhome = zswitch;
    returnHome();
    this->pos.setX(0.0);
    this->pos.setY(0.0);
    this->pos_z = 0.0;
    this->xyError.setX(0.0);
    this->xyError.setY(0.0);
    this->zError = 0.0;
}

Interpreter::~Interpreter()
{
    returnHome();
    delete this->sX;
    delete this->sY;
    delete this->drillToolEnb;
    if (this->sZ != NULL)
        delete this->sZ;
    delete this->xhome;
    delete this->yhome;
    if (this->zhome != NULL)
        delete this->zhome;
}

void Interpreter::setFeedrate(double fr)
{
    //TODO -- check for min/max feedrates
    this->feedrate = fr;
    this->step_delay_xy = (long) ((60L * 1000000L * UNITS_PER_STEP_XY) / this->feedrate);
    if (this->sZ != NULL)
        this->step_delay_z = (long) ((60L * 1000000L * UNITS_PER_STEP_Z) / this->feedrate);

#if DEBUG
    cout<<"----- setFeedrate ----- "<<endl;
    cout<<"\tfeedrate: "<<fr<<endl;
    cout<<"\tstep_delay_xy: "<<this->step_delay_xy<<"us"<<endl;
    cout<<"\tstep_delay_z: "<<this->step_delay_xy<<"us"<<endl;
#endif
}

void Interpreter::returnHome(void)
{
#if CHECK_LIMIT_SW
#if DEBUG
    cout<<"----- returnHome -----"<<endl;
#endif

    //kill power to all the motors first
    this->sX->killPower();
    this->sY->killPower();
    if (this->sZ != NULL) {
        this->sZ->killPower();
    }
    this->drillToolEnb->setVal(0);

    setFeedrate(10);
    //step z motor upwards until switch set
    if (this->zhome != NULL) {
        while (1) {
            if (this->zhome->getVal() == 0) {
                break;
            }
            this->sZ->step(1);
            usleep(step_delay_z);
        }
#if DEBUG
    cout<<"Z home"<<endl;
#endif
        this->zError = this->pos_z;
        this->sZ->killPower();
    }

    //step X motor backwards until switch set
    while (1) {
        if (this->xhome->getVal() == 0) {
            break;
        }
        this->sX->step(-1);
        usleep(step_delay_xy);
    }
#if DEBUG
    cout<<"X home"<<endl;
#endif
    this->xyError.setX(this->pos.getX());
    this->sX->killPower();

    //step y motor backwards until switch set
    while (1) {
        if (this->yhome->getVal() == 0) {
            break;
        }
        this->sY->step(-1);
        usleep(step_delay_xy);
    }
#if DEBUG
    cout<<"Y home"<<endl;
#endif
    this->sY->killPower();

    this->pos.setX(0.0);
    this->pos.setY(0.0);
    this->xyError.setX(0.0);
    this->xyError.setY(0.0);
    this->pos_z = 0;
    this->zError = 0;
#endif //CHECK_LIMIT_SW
}

void Interpreter::moveXY(Point next)
{
    int dx,dy;
    int dirx, diry;
    int steps, err;

    this->sX->killPower();
    this->sY->killPower();
    this->sZ->killPower();

    //calculate how many steps to take in X/Y direction
    //TODO -- consider X/Y error in dx calculation?
    dx = (int) round(((next.getX() - this->pos.getX()) / UNITS_PER_STEP_XY));
    dy = (int) round(((next.getY() - this->pos.getY()) / UNITS_PER_STEP_XY));

    dirx = dx > 0 ? 1 : -1;
    diry = dy > 0 ? 1 : -1;

    dx=abs(dx);
    dy=abs(dy);

#if DEBUG
    cout<<"----- moveXY ----- "<<endl;
    cout<<"\tcurrent pos: "<<this->pos.toString()<<endl;
    cout<<"\tnext pos: "<<next.toString()<<endl;
    cout<<"\tdx: "<<dx<<" steps, dirx: "<<dirx<<endl;
    cout<<"\tdy: "<<dy<<" steps, diry: "<<diry<<endl;
#endif

    if ((dx == 0) && (dy == 0)) //same point or too close, just return
        return;
    else if (dx == 0) { //only move up/down
        this->sX->killPower();
        steps = dy;
        while (steps > 0) {
            if ((this->yhome->getVal() == 0) &&
                (diry < 0) && (steps > 3)) {
                cerr<<"ERROR: trying to step Y past limit switch!"<<endl;
                this->sY->killPower();
                return;
            }
            this->sY->step(diry);
            usleep(step_delay_xy);
            steps--;
        }
    } else if (dy == 0) { //only move left/right
        this->sY->killPower();
        steps = dx;
        while (steps > 0) {
            if ((this->xhome->getVal() == 0) &&
                (dirx < 0) && (steps > 3)) {
                cerr<<"ERROR: trying to step X past limit switch!"<<endl;
                this->sX->killPower();
                return;
            }
            this->sX->step(dirx);
            usleep(step_delay_xy);
            steps--;
        }
    //Bresenham's line algorithm for x&y movement
    } else if (dx > dy) { //more x steps than y
        steps = dx;
        while (steps > 0) {
            if ((this->xhome->getVal() == 0) &&
                (dirx < 0) && (steps > 3)) {
                cerr<<"ERROR: trying to step X past limit switch!"<<endl;
                this->sY->killPower();
                return;
            } else if ((this->yhome->getVal() == 0) &&
                (diry < 0) && (steps > 3)) {
                cerr<<"ERROR: trying to step Y past limit switch!"<<endl;
                this->sY->killPower();
                return;
            }
            this->sX->step(dirx);
            usleep(step_delay_xy);
            this->sX->killPower();
            err += dy;
            if (err >= dx) {
                err -= dx;
                this->sY->step(diry);
                usleep(step_delay_xy);
                this->sY->killPower();
            }
            steps--;
        }
    } else { //more y steps than x
        steps = dy;
        while (steps > 0) {
            if ((this->xhome->getVal() == 0) &&
                (dirx < 0) && (steps > 3)) {
                cerr<<"ERROR: trying to step X past limit switch!"<<endl;
                this->sY->killPower();
                return;
            } else if ((this->yhome->getVal() == 0) &&
                (diry < 0) && (steps > 3)) {
                cerr<<"ERROR: trying to step Y past limit switch!"<<endl;
                this->sY->killPower();
                return;
            }
            this->sY->step(diry);
            usleep(step_delay_xy);
            this->sY->killPower();
            err += dx;
            if (err >= dy) {
                err -= dy;
                this->sX->step(dirx);
                usleep(step_delay_xy);
                this->sX->killPower();
            }
            steps--;
        }
    }

    this->pos.setX(this->pos.getX() + ((dx * dirx) * UNITS_PER_STEP_XY));
    this->pos.setY(this->pos.getY() + ((dy * diry) * UNITS_PER_STEP_XY));
    this->xyError.setX(next.getX() - this->pos.getX());
    this->xyError.setY(next.getY() - this->pos.getY());

#if DEBUG
    cout<<"\tnew pos: "<<this->pos.toString()<<endl;
    cout<<"\txy error: "<<this->xyError.toString()<<endl;
#endif


}

void Interpreter::moveZ(double next_z)
{
    setFeedrate(10);
    int dz, dirz, steps;
    if (this->sZ == NULL) //if no Z stepper declared, just return
        return;
    dz = (int) round(((next_z - this->pos_z) / UNITS_PER_STEP_Z));
    dirz = dz > 0 ? 1 : -1;

#if DEBUG
    cout<<"----- moveZ -----"<<endl;
    cout<<"\tcurrent z: "<<this->pos_z<<endl;
    cout<<"\tnext z: "<<next_z<<endl;
    cout<<"\tdz: "<<dz<<" steps, dirz: "<<dirz<<endl;
#endif

    //kill power to other motors first
    this->sX->killPower();
    this->sY->killPower();

    steps = abs(dz);
    while (steps > 0) {
        if ((this->zhome->getVal() == 0) &&
            (dirz > 0) && (steps > 3)) {
            cerr<<"ERROR: trying to step Z past limit switch!"<<endl;
            this->sZ->killPower();
            return;
        }
        this->sZ->step(dirz);
        usleep(step_delay_z);
        steps--;
    }
    this->pos_z += (dz * UNITS_PER_STEP_Z);
    this->zError = next_z - this->pos_z;

#if DEBUG
    cout<<"\tnew z: "<<this->pos_z<<endl;
    cout<<"\tz error: "<<this->zError<<endl;
#endif

    //kill power in z once it has stopped
    this->sZ->killPower();

    setFeedrate(this->feedrate);
}


bool Interpreter::processCmd(string cmd)
{
    bool rc;
    char gcmd1, gcmd2;
    double num, next_x, next_y, next_z;
    Point nextXY;
    stringstream ss;

    rc = true;

#if DEBUG
    cout<<"----- processCmd -----"<<endl;
    cout<<"\tcmd: "<<cmd<<endl;
#endif

    ss << cmd;
    ss >> gcmd1; //get first char
    switch(gcmd1) {
    case 'G':
        ss>>num;
        switch((int) num) {
        case 0: //G00 -- rapid linear move, absolute
#if DEBUG
            cout<<"\tG00 -- rapid linear move, absolute"<<endl;
#endif
            ss>>gcmd1>>next_x>>gcmd2>>next_y;
            if ((gcmd1 == 'X') && (gcmd2 == 'Y')) {
                nextXY.setX(next_x);
                nextXY.setY(next_y);
                moveXY(nextXY);
            } else if (gcmd1 == 'Z') {
                this->drillToolEnb->setVal(0); //turn off tool for retract
                moveZ(next_x);
            } else
                rc = false;
            break;
        case 1: //G01 -- linear move, relative
            this->drillToolEnb->setVal(1);
#if DEBUG
            cout<<"\tG01 -- linear move, relative"<<endl;
#endif
            ss>>gcmd1>>next_z;
            if (gcmd1 == 'Z')
                moveZ(next_z);
            else
                rc = false;
            break;
        case 4: //G04 -- dwell
           ss>>gcmd1>>num;
#if DEBUG
            cout<<"\tG04 -- dwell"<<endl;
            cout<<"\t\tdwell for "<<num<<"s"<<endl;
#endif
            if (gcmd1 == 'P')
               usleep(num * 1000000L);
            else
                rc = false;
            break;
        case 20: //G20 -- units in inches
#if DEBUG
            cout<<"\tG20 -- units in inches"<<endl;
#endif
           this->units = UNITS_INCH;
           break;
        case 21: //G21 -- units in mm
#if DEBUG
            cout<<"\tG20 -- units in mm"<<endl;
#endif
            this->units = UNITS_MM;
            break;
        case 64: //G64 -- continuous mode [with path tolerance]
#if DEBUG
            cout<<"\tG64 -- continuous mode [with path tolerance]"<<endl;
#endif
            //TODO -- how to implement this?
            break;
        case 90: //G90 -- absolute distance mode
#if DEBUG
            cout<<"\tG20 -- absolute distance mode"<<endl;
#endif
           this->dist_mode = DIST_MD_ABS;
           break;
        case 94: //G94 -- feedrate in units/min
#if DEBUG
            cout<<"\tG94 -- feedrate in units/min"<<endl;
#endif
            this->feedrate_mode = FEEDRATE_MD_UPM;
            break;
        } //end case(G)
        break;
    case 'X': //X[num] Y[num] --move to (X,Y)
        ss>>next_x>>gcmd2>>next_y;
        if ((gcmd1 == 'X') && (gcmd2 == 'Y')) {
            nextXY.setX(next_x);
            nextXY.setY(next_y);
            moveXY(nextXY);
        } else
            rc = false;
        break;
    case 'F':
        ss>>num;
        setFeedrate(num);
        break;
    case 'M':
        ss>>num;
        switch ((int) num) {
        case 2: //M2 -- program end
            returnHome();
            break;
        case 3: //M3 -- spindle on (CW)
#if DEBUG
            cout<<"\tM3 -- spindle on (clockwise)"<<endl;
#endif
            this->drillToolEnb->setVal(1);
            break;
        case 5: //M5 -- spindle off
#if DEBUG
            cout<<"\tM5 -- spindle off"<<endl;
#endif
            this->drillToolEnb->setVal(0);
            break;
        }
        break;
    default:
        break;
    }

    return rc;
}
