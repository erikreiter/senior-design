#include <cstdlib>
#include <iostream>
#include <fstream>
#include <signal.h>
#include <string>

#include "Interpreter/Interpreter.h"
#include "RpiGPIO/RpiGPIO.h"
#include "Stepper/Stepper.h"

#define PROG_NAME "etchpcb"
#define OUTPUT_FILE_PATH "./"

using namespace std;

void displayHelp(void);
void sig_handler(int sig);

bool stop_exec = false;

int main(int argc, char *argv[])
{
    string cmdStr = "";
    string outputPath = OUTPUT_FILE_PATH;
    string frontPath = "";
    string backPath = "";
    string drillPath = "";
    string millprojectPath = "";
    string filePath = "";
    string str = "";
    ifstream f;
    int i;
    int rc = 0;

    //signal for ctrl-C termination
    struct sigaction sa;
    sa.sa_handler = sig_handler;
    sa.sa_flags = 0;
    sigemptyset(&sa.sa_mask);

    Stepper *stepx = new Stepper(513, "5", "6", "13", "19");
    Stepper *stepy = new Stepper(513, "12", "16", "20", "21");
    RpiGPIO *switchx = new RpiGPIO("2", "in");
    RpiGPIO *switchy = new RpiGPIO("3", "in");
    Interpreter interpreter(stepx, stepy, switchx, switchy);

    //configure signal
    if (sigaction(SIGINT, &sa, NULL) == -1) {
        cout<<"Problem setting up sigaction"<<endl;
        return -1;
    }

    //parse input string for file paths
    for(i=1; i < argc; i++) {
        if (string(argv[i]) == "-h") {
            displayHelp();
            return 1;
        } else if (string(argv[i]) == "-m") {
            millprojectPath = argv[i+1];
            i++;
        } else if (string(argv[i]) == "-f") {
            frontPath = argv[i+1];
            i++;
        } else if (string(argv[i]) == "-b") {
            backPath = argv[i+1];
            i++;
        } else if (string(argv[i]) == "-d") {
            drillPath = argv[i+1];
            i++;
        } else if (string(argv[i]) == "-o") {
            outputPath = argv[i+1];
            i++;
        } else {
            cout<<"Unknown parameter: "<<argv[i]<<endl;
            displayHelp();
            return -1;
        }
    }
    if (millprojectPath == "") {
        cout<<"ERROR: millproject file is required"<<endl;
        displayHelp();
        return -1;
    }

    //copy millproject to current dir
    cmdStr = "cp " + millprojectPath + " ./millproject";
    millprojectPath = "./millproject";
    system(cmdStr.c_str());
    if (rc != 0) {
        cout<<"Error executing command: "<<cmdStr<<endl;
        return -1;
    }

    //create pcb2gcode and execute
    cmdStr = "pcb2gcode --output-dir " + outputPath + " ";
    if (frontPath != "")
        cmdStr += "--front " + frontPath + " ";
    if (backPath != "")
        cmdStr += "--back " + backPath + " ";
    if (drillPath != "")
        cmdStr += "--drill " + drillPath + " ";
    rc = system(cmdStr.c_str());
    if (rc != 0) {
        cout<<"Error executing command: "<<cmdStr<<endl;
        return -1;
    }
    //delete millproject in current dir
    cmdStr = "rm " + millprojectPath;
    rc = system(cmdStr.c_str());
    if (rc != 0) {
        cout<<"Error executing command: "<<cmdStr<<endl;
        return -1;
    }

    //process each gcode file
    if (frontPath != "") {
        cout<<"Ready to etch front..."<<endl;
        cout<<"Continue (y/n): "<<endl;
        getline(cin, str);
        if ((str == "y") || (str == "Y")) {
            cout<<"Etching front..."<<endl;
            filePath = outputPath + "front.ngc";
            f.open(filePath.c_str());
            if (!f.is_open()) {
                cout<<"could not open: "<<filePath<<"... exiting"<<endl;
                return 0;
            } else {
                while (getline(f, str)) {
                    if (stop_exec) {
                        cout<<"Terminating..."<<endl;
                        return -1;
                    }
                    interpreter.processCmd(str);
                }
                f.close();
            }
        } else {
            cout<<"Exiting..."<<endl;
            return 0;
        }
    }

    return 0;
}

void displayHelp(void)
{
    cout<<PROG_NAME<<" -- Usage:"<<endl;
    cout<<"\t-h: display help"<<endl;
    cout<<"\t-m <file>: millproject file path"<<endl;
    cout<<"\t-f <file>: front gerber file path"<<endl;
    cout<<"\t-b <file>: back gerber file path"<<endl;
    cout<<"\t-d <file>: drill gerber file path"<<endl;
    cout<<"\t-o <path>: output file directory path"<<endl;

}

void sig_handler(int sig)
{
    stop_exec = true;
}

