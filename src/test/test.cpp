#include <iostream>

#include "RpiGPIO/RpiGPIO.h"
#include "Stepper/Stepper.h"

using namespace std;

int main(void)
{
    RpiGPIO *pin3;
    Stepper step1(513, "5", "6", "13", "19"); 

    cout<<"testing..."<<endl;

    cout<<"initializing GPIO pin 3"<<endl;
    pin3 = new RpiGPIO("3");

    cout<<"exporting pin 3"<<endl;
    if (!pin3->exportPin())
        return -1;

    cout<<"setting pin 3 to output"<<endl;
    if (!pin3->setDir("out"))
        return -1;

    cout<<"setting pin 3 to high"<<endl;
    if (!pin3->setVal("1"))
        return -1;

    cout<<"testing stepper"<<endl;
    step1.setSpeed(30);
    while (1) {
        step1.step(-1);
    }

    return 1;
}
