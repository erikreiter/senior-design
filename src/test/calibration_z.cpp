#include <iostream>
#include <string>
#include <unistd.h>

#include "../RpiGPIO/RpiGPIO.h"
#include "../Stepper/Stepper.h"

#define UNITS_PER_STEP_Z  0.000246

int steps_taken;
float distance_traveled;

void printSummary(void);

int main(void)
{
    Stepper *stepz = new Stepper(200, 22, 10, 9, 11);
    RpiGPIO *switchz = new RpiGPIO(17, "in");

    string input = "";
    string::size_type sz;

    cout<<"Z Stepper Calibration -- Inches Per Step: "<<UNITS_PER_STEP_Z<<endl;
    cout<<"Commands:"<<endl;
    cout<<"\texit -- close program"<<endl;
    printSummary();

    while (1) {
        if (switchz->getVal() == 0) {
            cout<<"Switch closed!"<<endl;
        }

        cout<<"Steps to Take: ";

        getline(cin, input);
        if (input == "exit") {
            cout<<"Exiting..."<<endl;
            printSummary();
            break;
        }

        try {
            int dir;
            int steps = std::stoi(input, &sz);

            steps > 0 ? dir = 1 : dir = -1;

            if (dir == -1)
                steps = steps * -1;

            steps_taken += (steps * dir);
            distance_traveled += (steps * dir) * UNITS_PER_STEP_Z;
            while (steps > 0) {
                stepz->step(dir);
                usleep(1476);
                steps--;
            }
            printSummary();
        } catch (exception) {
            cout<<"Invalid input"<<endl;
        }
    }

    delete stepz;
}

void printSummary(void)
{
    cout<<"-- Summary --"<<endl;
    cout<<"\tSteps taken: "<<steps_taken<<endl;
    cout<<"\tDistance traveled: "<<distance_traveled<<endl;
}
